<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Behat\Hook\Scope\AfterStepScope;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends  MinkContext implements Context, SnippetAcceptingContext
{
  

  /**
   * Screnshot do teste
   * Works only with Selenium2Driver.
   *
   * @AfterStep
   */
  public function takeScreenShotAfterFailedStep(afterStepScope $scope)
  {
    if (99 === $scope->getTestResult()->getResultCode()) {

      $driver = $this->getSession()->getDriver();

      if (!($driver instanceof Selenium2Driver)) {
        return;
      }
      
      $oStepNode = $scope->getStep();
      $iLine     = $oStepNode->getLine();
      $oScenarioNode = $scope->getName();
      $sScenario     = $scope->getFeature()->getTitle();
      $sErroName     = str_replace(array(".", " ", "/", "\""), "_", $sScenario . "_" . $oStepNode->getText());
      $screenshot    = $driver->getWebDriverSession()->screenshot();
     
      $path = dirname(dirname(__FILE__)). DIRECTORY_SEPARATOR . 'screenshot'. DIRECTORY_SEPARATOR . $sErroName .'.png';
  
      $this->getSession()->evaluateScript("document.body.style.border='3px solid #ff0000'"); 

      file_put_contents($path , $this->getSession()->getDriver()->getScreenshot());
    }
  }

}
